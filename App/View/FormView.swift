//
//  FormView.swift
//  App
//
//  Created by GIC-Admin on 8/24/23.
//  Copyright © 2023 GIC-Admin. All rights reserved.
//

import UIKit

// Use this protocol to create function(interface) for controller
protocol FormViewDelegate: AnyObject {
    func submit()
    func pickImage()
}

class FormView: UIView {

//    var test: (() -> Void)?
    
//    private var label: UILabel!
//    private var textView: UITextView!

    private var profile: UIImageView!
    private var cameraImageV: UIImageView!
    private var control: UIControl!
    
    private var button: UIButton!
    // weak variable delegrate when memory clear it will clear too
    weak var delegate: FormViewDelegate?
    
    init() {
        super.init(frame: .zero)
        
        profile = UIImageView()
        profile.translatesAutoresizingMaskIntoConstraints = false
        profile.image = UIImage(named: "profile")
        
        cameraImageV = UIImageView()
        cameraImageV.translatesAutoresizingMaskIntoConstraints = false
        cameraImageV.image = UIImage(named: "camera")
        
        control = UIControl()
        control.translatesAutoresizingMaskIntoConstraints = false
        control.addTarget(self, action: #selector(pickImage), for: .touchUpInside)
        
        addSubview(profile)
        addSubview(cameraImageV)
        addSubview(control)
        
        profile.widthAnchor.constraint(equalToConstant: 150).isActive = true
        profile.heightAnchor.constraint(equalToConstant: 150).isActive = true
        profile.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        profile.bottomAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        cameraImageV.widthAnchor.constraint(equalToConstant: 30).isActive = true
        cameraImageV.heightAnchor.constraint(equalToConstant: 30).isActive = true
        cameraImageV.centerXAnchor.constraint(equalTo: profile.centerXAnchor).isActive = true
        cameraImageV.bottomAnchor.constraint(equalTo: profile.bottomAnchor,
                                             constant: -5).isActive = true
        
        control.topAnchor.constraint(equalTo: profile.topAnchor).isActive = true
        control.bottomAnchor.constraint(equalTo: profile.bottomAnchor).isActive = true
        control.leadingAnchor.constraint(equalTo: profile.leadingAnchor).isActive = true
        control.trailingAnchor.constraint(equalTo: profile.trailingAnchor).isActive = true
               
//        label = UILabel()
//        label.translatesAutoresizingMaskIntoConstraints = false
//        label.textAlignment = .center
//        label.text = " textView = UITextView() textView.translatesAutoresizingMaskIntoConstraints = false textView.backgroundColor = .lightGraytextView.layer.cornerRadius = 6 textView.layer.borderColor = UIColor.darkGray.cgColor   textView.layer.borderWidth = 1"
//        label.numberOfLines = 0
        
//        textView = UITextView()
//        textView.translatesAutoresizingMaskIntoConstraints = false
//        textView.backgroundColor = .lightGray
//        textView.layer.cornerRadius = 6
//        textView.layer.borderColor = UIColor.darkGray.cgColor
//        textView.layer.borderWidth = 1
//        textView.isEditable = false
        
        button = UIButton(type: .system)
        button.setTitle("Submit", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 6
        button.addTarget(self, action: #selector(submit), for: .touchUpInside)
        
//        addSubview(label)
//        addSubview(textView)
        addSubview(button)
        
//        label.leadingAnchor.constraint(equalTo: textView.leadingAnchor).isActive = true
//        label.trailingAnchor.constraint(equalTo: textView.trailingAnchor).isActive = true
//        label.bottomAnchor.constraint(equalTo: textView.topAnchor, constant: -15).isActive = true
//
        
        // self = FormView parent
//        textView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 40).isActive = true
//        textView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -40).isActive = true
//        textView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//        textView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        button.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        button.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -40).isActive = true
        button.heightAnchor.constraint(equalToConstant: 45).isActive = true
        button.widthAnchor.constraint(equalToConstant: 120).isActive = true
        
//        let tabGus = UITapGestureRecognizer(target: self, action: #selector(closeKeyboard))
//
//        addGestureRecognizer(tabGus)
        
        // Set the image corner to rounded
        profile.layer.masksToBounds = true
        control.layer.masksToBounds = true

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override internal func layoutSubviews() {
        super.layoutSubviews()
        
        profile.layer.cornerRadius = profile.frame.width / 2
        control.layer.cornerRadius = profile.frame.width / 2
    }
    
    func setImage(_ image: UIImage){
        profile.image = image
    }
    
    @objc private func submit(){
//        delegate?.submit(textView.text)
    }
//
//    @objc private func closeKeyboard(){
//        textView.resignFirstResponder()
//    }
    
    @objc private func pickImage(){
        delegate?.pickImage()
    }
    
}
