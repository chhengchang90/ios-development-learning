//
//  FormViewController.swift
//  App
//
//  Created by GIC-Admin on 8/24/23.
//  Copyright © 2023 GIC-Admin. All rights reserved.
//

import UIKit

class FormViewController: UIViewController {
    
    private var formView: FormView!

    override func loadView() {
        super.loadView()
                
        formView = FormView()
        formView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(formView)
        
        formView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        formView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        formView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        formView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
//        formView.frame = view.frame
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        view.backgroundColor = .white
        formView.delegate = self
    }
    
    private func openPhotoAlbume(){
        let imagePicker = UIImagePickerController()
        
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
//        imagePicker.allowsEditing = false
    
        present(imagePicker, animated: true)
    }
    
    private func opernCamera(){
        let imagePicker = UIImagePickerController()
        
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
//        imagePicker.allowsEditing = false

        present(imagePicker, animated: true)
    }
}

// Mark: - Image Picker delegate
extension FormViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
		picker.dismiss(animated: true, completion: nil)

        guard let image = info[.orignalImage] as? UIImage else { return }

        formView.setImage(image)
    }
}

// Mark: - Form View delegate
extension FormViewController: FormViewDelegate{
    
    func pickImage() {
        // open action
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: "Camera", style: .default) { _ in
            self.opernCamera()
        }
        
        let photo = UIAlertAction(title: "Photo", style: .default) { _ in
            self.openPhotoAlbume()
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel)
        
        actionSheet.addAction(camera)
        actionSheet.addAction(photo)
        actionSheet.addAction(cancel)
        
        present(actionSheet, animated: true)
        
    }
    
    
    func submit() {

    }
    
    
    
}
