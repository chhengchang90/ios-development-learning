//
//  Contact.swift
//  App
//
//  Created by GIC-Admin on 8/23/23.
//  Copyright © 2023 GIC-Admin. All rights reserved.
//

import Foundation

struct Contact {
    var profile: String = ""
    var name: String = ""
    var status: String = ""
    var phone: String = ""
}
